# Linear Regression

## ft_linear_regression


Here is an implementation of the ecole42 tutorial project on linear regression.

The initial data should be a csv file containing two columns with data on the price of the car and its mileage. Based on this data, linear_regression.py first calculates the coefficients thetta_0 and thetta_1 (standard deviation), and then prompts you to enter the mileage value for price prediction. Data must be in .csv format (comma separated values). And contain two named columns: mileage and estimate price.


![Screenshots](/docs/graph.png)

[Subject](/docs/en.subject.ft_linear_regression.pdf)

[Linear Regression - Wiki](https://en.wikipedia.org/wiki/Linear_regression)



Makefile contains command for installing from apt `sys_req.txt` and from pip3 `req.txt`.

Type make for install:

```sh
> make

```

For training, like here:

```sh
> python3 linear_regression.py --file data_file_name.csv --epsilon float_value_of_accuracy --learningRate float_value_of_learning_rate --autoLearningRate --graphic
Training completed!

Total Iteration 808
Enter mileage for predict price or 'q' for exit: 12345.6
For  12345.6  price is  8477.925850837706
Enter mileage for predict price or 'q' for exit: q       
You can use estimate_price.py
Exit.
```

After training, the coefficients are saved to the params.csv file and can be used after exiting the program by running

```sh
> python3 estimate_price.py --file file_name_with_parameters
Enter mileage for predict price or 'q' for exit: 123456.7
For  123456.7  price is  5859.8648864846145
Enter mileage for predict price or 'q' for exit: q
```

Help:
```sh
> python3 linear_regression.py --help
usage: linear_regression.py [-h] [-f FILE_NAME] [-eps EPSILON] [-lr LEARNING_RATE] [-graph]
                            [-graphData] [-autoLR]

Input file name containing training data to the program to train the model

optional arguments:
  -h, --help            show this help message and exit
  -f FILE_NAME, --file FILE_NAME
                        set data file name
  -eps EPSILON, --epsilon EPSILON
                        set epsilon
  -lr LEARNING_RATE, --learningRate LEARNING_RATE
                        set learning rate
  -graph, --graphic     for display graphic
  -graphData, --graphDataOnly
                        for display graphic with data only
  -autoLR, --autoLearningRate
                        for learning rate auto decreasing

```

Our team stores the [next project](https://gitlab.com/saltyivan/dslr) along
this route (Graphic 3D-render) in the Team Lead account on Gitlab
