import os.path as path
import numpy as np
import sys
import matplotlib.pyplot as plt
import argparse
from math import fsum
from parserCSV import parse

class lin_reg(object):

    def __init__(self):
        self.thetta = np.array([.0, .0])
        self.learning_rate = 1e-2
        self.epsilon = 1e-6
        self.autoLR = False
        self.total_iteration = 0
        self.graphic = False
        self.gr_data = False
        return

    def load_model(self, filename):
        if type(filename) is not str:
            print("Error: expected file name type is str, given: ", type(value), file = sys.stderr)
            exit(-2)
        if not path.isfile(filename):
            print("Error: file does not exist.", file = sys.stderr)
            exit(-3)
        with open(filename) as file:
            line = file.readline()
            try:
                self.thetta = [float(x) for x in line.split(",")]
            except:
                print("Error: expected int or float value type in the parameters file. Wrong convertation.", file = sys.stderr)
                exit(-4)
            if file.readline() != "":
                print("Warning: readed only single line in the parameters file, other lines are skipped.", file = sys.stderr)
            return
        print("Error: file access error.", file = sys.stderr)
        exit(-5)

    def set_epsilon(self, epsilon):
        if type(epsilon) != float and type(epsilon) != int:
            print("Error: expected epsilon type is int or float, given: ", type(value), file = sys.stderr)
            exit(-2)
        self.epsilon = epsilon
        return
		
    def set_learning_rate(self, learning_rate):
        if type(learning_rate) != float and type(learning_rate) != int:
            print("Error: expected learning rate type is int or float, given: ", type(learning_rate), file = sys.stderr)
            exit(-2)
        self.learning_rate = learning_rate
        return

    def set_autoLR(self, value):
        if type(value) is not bool:
            print("Error: expected parameter type is bool, given: ", type(value), file = sys.stderr)
            exit(-2)
        self.autoLR = value
    
    def set_gr_data(self, value):
        if type(value) is not bool:
            print("Error: expected parameter type is bool, given: ", type(value), file = sys.stderr)
            exit(-2)
        self.gr_data = value      

    def set_graph(self, value):
        if type(value) is not bool:
            print("Error: expected parameter type is bool, given: ", type(value), file = sys.stderr)
            exit(-2)
        self.graphic = value

    def estimate_price(self, mileage):
        if type(mileage) is not float and type(mileage) is not int:
            print("Error: expected mileage type is int or float, given: ", type(mileage), file = sys.stderr)
            exit(-2)
        return self.thetta[0] + self.thetta[1] * mileage

    def train_data(self, filename):
        if type(filename) is not str:
            print("Error: expected string as filename, given: ", type(filename[0]), file = sys.stderr)
            exit(-2)
        first_line = True
        self.all_data = parse(fileName = filename, countColumns = 2, header = True)[0]
        self.all_data.sort(key = lambda x: x[0])
        self.min_max_x = [self.all_data[0], self.all_data[len(self.all_data) - 1]]
        half = len(self.all_data) // 2
        lst = [[[int(x[0]), int(x[1])] for x in self.all_data[i : i + half]] for i in range(0, len(self.all_data), half)]
        p = np.array([[fsum([x[i] for x in lst[j]]) // half for i in range(2)] for j in range(2)])
        dp = p[0] - p[1]
        self.thetta_new = np.array([np.linalg.det(p) / dp[0], dp[1] / dp[0]], dtype = np.float64)
        return

    def compute_thetta(self):
        thetta_new = self.thetta_new
        iteration = 0
        while True:
            iteration += 1
            if (np.isnan(thetta_new[0]) or np.isnan(thetta_new[1]) or np.isinf(thetta_new[0]) or np.isinf(thetta_new[1])):
                self.total_iteration += iteration
                return True
            self.thetta = thetta_new
            sum = np.array([.0, .0], dtype = np.float64)
            for distance, price in self.all_data:
                sum += (self.estimate_price(distance) - price) * np.array([1.0, distance], dtype = np.float64)
            thetta_new = np.array([(x - self.learning_rate * y / len(self.all_data)) for x, y in zip(self.thetta, sum)])
            if (all([x < 0 for x in np.absolute(self.thetta - thetta_new) - np.array([self.epsilon, self.epsilon])])):
                self.total_iteration += iteration
                self.thetta = thetta_new
                return 

    def fit(self, filename):
        self.train_data(filename)
        self.total_iteration = 0
        np.seterr(all = "ignore")
        if self.autoLR:
            while self.compute_thetta():
                self.learning_rate /= 10
        else:
            if self.compute_thetta():
                print("This sequence didn't fit! Select autoLR mod (-autoLR) "
                        + "or decrease the learning rate.")
                exit()
        with open("params.csv", "w") as f:
            f.write(str(self.thetta[0]) + ", " + str(self.thetta[1]))
        if self.graphic or self.gr_data:
            self.graph()

    def graph(self, *filename):
        data_x = [x[0] for x in self.all_data]
        data_y = [x[1] for x in self.all_data]
        extra = [[min(data_x), max(data_x)], [min(data_y), max(data_y)]] 
        x = np.linspace(extra[0][0], extra[0][1], int(max(extra[0][1] - extra[0][0], extra[1][1] - extra[1][0])))
        y = self.thetta[0] + self.thetta[1] * x
        if self.graphic:
            plt.plot(x, y, "b", label = "linear regression")
        plt.plot(data_x, data_y, "ro", label = "source data")
        plt.legend()
        plt.axis([extra[0][0], extra[0][1], extra[1][0], extra[1][1]])
        plt.show()

