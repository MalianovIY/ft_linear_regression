import os.path as path
import sys
import argparse

from ft_linear_regression import lin_reg

arg_pr = argparse.ArgumentParser(description = "Input the file name containing parameters to the current model", add_help = True, conflict_handler = "resolve")
arg_pr.add_argument("-f", "--file", action = "store", type = str, dest = "file_name", help = "file name")
args = arg_pr.parse_args(sys.argv[1::])

if args.file_name is None:
    print("Error: Enter file name using -f (--file)")
    exit(-1)
else:
    lin_reg = lin_reg()
    lin_reg.load_model(args.file_name)
while True:
    try:
        comand = input("Enter mileage for predict price or 'q' for exit: ")
        if comand == "q":
            break
        mileage = float(comand)
        if mileage < 0:
            print("Warning: mileage can't be negative. R u dumb?", file = sys.stderr)
    except:
        if input("Input error\n\n" 
                +"Input 'retry' to continue or something else to exit: ") == "retry":
            continue
        else:
            exit()
    estimate = lin_reg.estimate_price(mileage)
    print("For ", mileage, " price is ", estimate)
    if estimate < 0:
        print("Warning: the answer can be false. I'm just a linear regression.\n"
                + "Hardly anyone will need your bucket of nuts, even if you pay for it",
                file = sys.stderr)

