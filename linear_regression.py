import os.path as path
import numpy as np
import sys
import matplotlib.pyplot as plt
import argparse
from math import fsum

from ft_linear_regression import lin_reg

arg_pr = argparse.ArgumentParser(description = "Input file name containing training data "
        + "to the program to train the model", 
        add_help = True, conflict_handler = "resolve")
arg_pr.add_argument("-f", "--file", action = "store", type = str, dest = "file_name", 
        help = "set data file name")
arg_pr.add_argument("-eps", "--epsilon", action = "store", type = float, 
        dest = "epsilon", help = "set epsilon")
arg_pr.add_argument("-lr", "--learningRate", action = "store", type = float,
        dest="learning_rate", help = "set learning rate")
arg_pr.add_argument("-graph", "--graphic", action = "store_true", 
        dest = "graph", help = "for display graphic")
arg_pr.add_argument("-graphData", "--graphDataOnly", action = "store_true", 
        dest = "gr_data", help = "for display graphic with data only")
arg_pr.add_argument("-autoLR", "--autoLearningRate", action = "store_true", 
        dest = "autoLR", help = "for learning rate auto decreasing")

arg_pr.set_defaults(graph = False, autoLR = False, gr_data = False)

args = arg_pr.parse_args(sys.argv[1::])

if args.file_name is None:
    print("Error: Enter the data file name using -f (--file)")
    exit(-1)
else:
    lin_reg = lin_reg()
    if args.epsilon is not None:
        lin_reg.set_epsilon(args.epsilon)
    if args.learning_rate is not None:
        lin_reg.set_learning_rate(args.learning_rate)
    if args.autoLR:
        lin_reg.set_autoLR(True)
    if args.gr_data:
        lin_reg.set_gr_data(True)
    if args.graph:
        lin_reg.set_graph(True)
    lin_reg.fit(args.file_name)
    print("Training completed!\n")
    print("Total Iteration", lin_reg.total_iteration)
while True:
    try:
        comand = input("Enter mileage for predict price or 'q' for exit: ")
        if comand == "q":
            print("You can use estimate_price.py\nExit.")
            break
        mileage = float(comand)
        if mileage < 0:
            print("Warning: mileage can't be negative. R u dumb?", file = sys.stderr)
    except:
        if input("Input error\n\n"
                + "Input 'retry' to continue or something else to exit: ") == "retry":
            continue
        else:
            print("You can use estimate_price.py\nExit.")
            exit()

    estimate = lin_reg.estimate_price(mileage)
    print("For ", mileage, " price is ", estimate)
    if estimate < 0:
        print("Warning: the answer can be false. I'm just a linear regression.\n" 
                + "Hardly anyone will need your bucket of nuts, even if you pay for it", file = sys.stderr)
