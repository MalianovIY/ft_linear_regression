import os.path as path
import sys

def parse(fileName, countColumns = 2, header = True):
    if not path.isfile(fileName):
        print("Error: file does not exist.", file = sys.stderr)
        exit(-3)
    with open(fileName, 'r') as f:
        if header:
            columnsName = f.readline().split(',')
            if len(columnsName) != countColumns:
                print("Error: expected", countColumns, 
                        " in the data file, contains ", 
                        len(columnsName), file = sys.stderr)
                exit(-1)
        else:
            columnsName = ['mileage', 'price']
        data = []
        for i, line in enumerate(f):
            lst = line.split('\n')[0].split(',')
            if len(lst) != countColumns:
                print("Error: expeccted", countColumns,
                        "in the data file, but the line ", i + 1, 
                        " contains ", len(lst), file = sys.stderr)
                exit(-4)
            try:
                data.append([float(x) for x in lst])
            except:
                print("Error: expected int or float value type in the "
                        + "data file. Wrong convert.", file = sys.stderr)
                exit(-2)
    return [data, columnsName]

